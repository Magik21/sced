package com.example.tim_6.sced;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class TeachersActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private List<DayOfWeek> DayOfWeek_List;
    private RecyclerView.Adapter adapter;
    SharedPreferences preferences;
    String setting_5_ext;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teachers);

        mList = findViewById(R.id.main_list);
        navigationView = (NavigationView) findViewById(R.id.nav_view_teach);

        DayOfWeek_List = new ArrayList<>();
        adapter = new DayOfWeek_Adapter(getApplicationContext(), DayOfWeek_List);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(adapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().findItem(R.id.nav_teach).setChecked(true);      //Выделение пункта в navigation menu
        setting_5_ext = preferences.getString("extra_Setting_5",getString(R.string.url_teach));
        getData();
    }


    private void getData() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Синхронизация...");
        progressDialog.show();

        DayOfWeek_List.clear();
        //Обработка отсутсвия сети
        DayOfWeek oneDay_ass = new DayOfWeek();
        oneDay_ass.setTitle("   Отсутствует подключение к серверу!");
        oneDay_ass.setCab("");
        DayOfWeek_List.add(oneDay_ass);
        adapter.notifyDataSetChanged();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, setting_5_ext, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    DayOfWeek_List.clear();
                    JSONObject jsonObject = response.getJSONObject("data");
                    JSONArray jsonArray = jsonObject.getJSONArray("teachers");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject_teach = jsonArray.getJSONObject(i);

                        DayOfWeek oneLes = new DayOfWeek();
                        oneLes.setTitle("== " + jsonObject_teach.getString("lesson"));                                                        //День недели
                        oneLes.setCab("");
                        DayOfWeek_List.add(oneLes);

                        DayOfWeek oneName = new DayOfWeek();
                        oneName.setTitle("" + jsonObject_teach.getString("name"));                                                        //День недели
                        oneName.setCab("");
                        DayOfWeek_List.add(oneName);

                        DayOfWeek oneDay_1 = new DayOfWeek();
                        oneDay_1.setTitle("");
                        oneDay_1.setCab("");
                        DayOfWeek_List.add(oneDay_1);

                        adapter.notifyDataSetChanged();
                        progressDialog.dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }




        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed = 10 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 7 * 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(new JSONObject(jsonString), cacheEntry);
                } catch (UnsupportedEncodingException | JSONException e) {
                    return Response.error(new ParseError(e));
                }
            }

            @Override
            protected void deliverResponse(JSONObject response) {
                super.deliverResponse(response);
            }

            @Override
            public void deliverError(VolleyError error) {
                progressDialog.dismiss();
                //Toast.makeText(getApplicationContext(), "Ахтунг! Критическая ошибка.", Toast.LENGTH_SHORT).show();
                super.deliverError(error);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                return super.parseNetworkError(volleyError);
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        final int id = item.getItemId();
        drawer.closeDrawers();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (id) {
                    case R.id.nav_time:
                        startActivity(new Intent(TeachersActivity.this, TimeActivity.class));
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.nav_sc:
                        startActivity(new Intent(TeachersActivity.this, ScheduleActivity.class));
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.nav_kn:
                        startActivity(new Intent(TeachersActivity.this, KnockActivity.class));
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.nav_teach:
                        drawer.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.nav_website:
                        startActivity(new Intent(TeachersActivity.this, WebsiteActivity.class));
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.nav_settings:
                        startActivity(new Intent(TeachersActivity.this, SettingsActivity.class));
                        break;
                }
            }
        }, 250);    //Задержка

        return true;
    }


}
