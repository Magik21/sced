package com.example.tim_6.sced;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.app.ProgressDialog;
import android.widget.Toast;
import android.view.MenuItem;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.util.Log;

public class KnockActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private List<DayOfWeek> DayOfWeek_List;
    private RecyclerView.Adapter adapter;
    NavigationView navigationView;
    SharedPreferences preferences;
    String setting_1_ext;
    String weekT[] = new String[11];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_knock);

        mList = findViewById(R.id.main_list);
        navigationView = (NavigationView) findViewById(R.id.nav_view_kn);

        DayOfWeek_List = new ArrayList<>();
        adapter = new TimeActivity_Adapter(getApplicationContext(), DayOfWeek_List);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(adapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        getData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().findItem(R.id.nav_kn).setChecked(true);      //Выделение пункта в navigation menu
        setting_1_ext = preferences.getString("extra_Setting_1",getString(R.string.url_timetable));
        getData();
    }


    private void getData() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Синхронизация...");
        progressDialog.show();

        DayOfWeek_List.clear();
        //Обработка отсутсвия сети
        DayOfWeek oneDay_ass = new DayOfWeek();
        oneDay_ass.setTitle("   Отсутствует подключение к серверу!");
        oneDay_ass.setCab("");
        DayOfWeek_List.add(oneDay_ass);
        adapter.notifyDataSetChanged();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, setting_1_ext, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    DayOfWeek_List.clear();
                    JSONObject jsonObject = response.getJSONObject("data");
                    JSONArray jsonTimetable = jsonObject.getJSONArray("timetable");
                    JSONArray jsonTimeWeekdays = jsonObject.getJSONArray("weekdays");
                    JSONArray jsonTimeWeekend = jsonObject.getJSONArray("weekend");

                    Calendar cal = Calendar.getInstance();
                    int dayOfWeek;
                    JSONObject jsonObject_day;

                    if (cal.get(Calendar.HOUR_OF_DAY) >= 15 && cal.get(Calendar.DAY_OF_WEEK) == 7) {           // Расписание на понедельник в субботу
                        dayOfWeek = 0;

                        jsonObject_day = jsonTimetable.getJSONObject(dayOfWeek);
                        DayOfWeek oneDay = new DayOfWeek();

                        oneDay.setTitle("Расписание на послезавтра (" + jsonObject_day.getString("day") + ").");                           //Шапка view
                        oneDay.setCab("");
                        DayOfWeek_List.add(oneDay);

                    } else if (cal.get(Calendar.HOUR_OF_DAY) >= 15 || cal.get(Calendar.DAY_OF_WEEK) == 1) {
                        dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 1;

                        jsonObject_day = jsonTimetable.getJSONObject(dayOfWeek);
                        DayOfWeek oneDay = new DayOfWeek();

                        oneDay.setTitle("Расписание на завтра (" + jsonObject_day.getString("day") + ").");                           //Шапка view
                        oneDay.setCab("");
                        DayOfWeek_List.add(oneDay);
                    } else {
                        dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 2;

                        jsonObject_day = jsonTimetable.getJSONObject(dayOfWeek);
                        DayOfWeek oneDay = new DayOfWeek();

                        oneDay.setTitle("Расписание на сегодня (" + jsonObject_day.getString("day") + ").");                           //Шапка view
                        oneDay.setCab("");
                        DayOfWeek_List.add(oneDay);
                    }

                    if(dayOfWeek != 5) {
                        for(int i = 0; i < jsonTimeWeekdays.length(); i++) {
                            weekT[i] = jsonTimeWeekdays.getJSONArray(i).getString(0)
                            + " - " + jsonTimeWeekdays.getJSONArray(i).getString(1)
                            + " [" + jsonTimeWeekdays.getJSONArray(i).getString(2) + "]";
                        }
                    } else {
                        for(int i = 0; i < jsonTimeWeekend.length(); i++) {
                            weekT[i] = jsonTimeWeekend.getJSONArray(i).getString(0)
                                    + " - " + jsonTimeWeekend.getJSONArray(i).getString(1)
                                    + " [" + jsonTimeWeekend.getJSONArray(i).getString(2) + "]";
                        }
                    }

                    JSONArray jsonArray_lessons = jsonObject_day.getJSONArray("lessons");

                    for (int j = 0; j < jsonArray_lessons.length(); j++) {

                        JSONObject jsonObject_lesson = jsonArray_lessons.getJSONObject(j);

                        DayOfWeek oneDay_ = new DayOfWeek();

                        oneDay_.setTitle(Integer.toString(j + 1) + ".  " + jsonObject_lesson.getString("subject"));
                        oneDay_.setCab(weekT[j]);

                        DayOfWeek_List.add(oneDay_);

                        adapter.notifyDataSetChanged();
                        progressDialog.dismiss();
                    }

                    DayOfWeek oneDay_1 = new DayOfWeek();
                    oneDay_1.setTitle("" + System.getProperty("line.separator") + "Будние");
                    oneDay_1.setCab("");
                    DayOfWeek_List.add(oneDay_1);

                    for(int a = 0; a < jsonTimeWeekdays.length(); a++) {
                        DayOfWeek weekDay_time = new DayOfWeek();
                        weekDay_time.setTitle(jsonTimeWeekdays.getJSONArray(a).getString(0)
                                + " - " + jsonTimeWeekdays.getJSONArray(a).getString(1)
                                + " [" + jsonTimeWeekdays.getJSONArray(a).getString(2) + "]");
                        weekDay_time.setCab("");
                        DayOfWeek_List.add(weekDay_time);
                    }



                    DayOfWeek oneDay_2 = new DayOfWeek();
                    oneDay_2.setTitle("" + System.getProperty("line.separator") + "Суббота");
                    oneDay_2.setCab("");
                    DayOfWeek_List.add(oneDay_2);

                    for(int a = 0; a < jsonTimeWeekend.length(); a++) {
                        DayOfWeek weekDay_time = new DayOfWeek();
                        weekDay_time.setTitle(jsonTimeWeekend.getJSONArray(a).getString(0)
                                + " - " + jsonTimeWeekend.getJSONArray(a).getString(1)
                                + " [" + jsonTimeWeekend.getJSONArray(a).getString(2) + "]");
                        weekDay_time.setCab("");
                        DayOfWeek_List.add(weekDay_time);

                    }

                    adapter.notifyDataSetChanged();
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed = 10 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 7 * 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(new JSONObject(jsonString), cacheEntry);
                } catch (UnsupportedEncodingException | JSONException e) {
                    return Response.error(new ParseError(e));
                }
            }

            @Override
            protected void deliverResponse(JSONObject response) {
                super.deliverResponse(response);
            }

            @Override
            public void deliverError(VolleyError error) {
                progressDialog.dismiss();
                //Toast.makeText(getApplicationContext(), "Ахтунг! Критическая ошибка.", Toast.LENGTH_SHORT).show();
                super.deliverError(error);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                return super.parseNetworkError(volleyError);
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        final int id = item.getItemId();
        drawer.closeDrawers();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (id) {
                    case R.id.nav_time:
                        startActivity(new Intent(KnockActivity.this, TimeActivity.class));
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.nav_sc:
                        startActivity(new Intent(KnockActivity.this, ScheduleActivity.class));
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.nav_kn:
                        drawer.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.nav_teach:
                        startActivity(new Intent(KnockActivity.this, TeachersActivity.class));
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.nav_website:
                        startActivity(new Intent(KnockActivity.this, WebsiteActivity.class));
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.nav_settings:
                        startActivity(new Intent(KnockActivity.this, SettingsActivity.class));
                        break;
                }
            }
        }, 250);    //Задержка

        return true;
    }


}
