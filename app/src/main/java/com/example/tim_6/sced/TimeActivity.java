package com.example.tim_6.sced;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.view.MenuItem;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Locale;
import java.text.SimpleDateFormat;


public class TimeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private List<DayOfWeek> DayOfWeek_List;
    private RecyclerView.Adapter adapter;
    private TextView textView_1;
    private TextView textView_2;
    int knockTime[] = new int[30];
    int knockTimeLength;
    String[] lessons = {"Урок", "Урок", "Урок", "Урок", "Урок", "Урок", "Урок", "Урок", "Урок"};
    String[] cabines = {"", "", "", "", "", "", "", "", ""};
    NavigationView navigationView;
    SharedPreferences preferences;
    boolean setting_2_def;
    String setting_1_ext;
    String setting_3_ext;
    private Timer mTimer;
    private MyTimerTask mMyTimerTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        textView_1 = (TextView) findViewById(R.id.time_left);
        textView_2 = (TextView) findViewById(R.id.next_item);
        navigationView = (NavigationView) findViewById(R.id.nav_view_time);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        mList = findViewById(R.id.time_list);

        DayOfWeek_List = new ArrayList<>();
        adapter = new TimeActivity_Adapter(getApplicationContext(), DayOfWeek_List);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(adapter);

        setting_3_ext = preferences.getString("extra_Setting_3", getString(R.string.url_version_api));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        dialogUpgrade();
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().findItem(R.id.nav_time).setChecked(true);      //Выделение пункта в navigation menu
        setting_2_def = preferences.getBoolean("Setting_2", false);
        setting_1_ext = preferences.getString("extra_Setting_1", getString(R.string.url_timetable));

        getData();

        mTimer = new Timer();
        mMyTimerTask = new MyTimerTask();
        try {
            Thread.sleep(150);
        } catch (Exception e) {
        }
        mTimer.schedule(mMyTimerTask, 0, 3000);

    }


    @Override
    protected void onPause() {
        super.onPause();
        mTimer.cancel();
    }

    private void getData() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Синхронизация...");
        progressDialog.show();

        DayOfWeek_List.clear();
        //Обработка отсутсвия сети
        DayOfWeek oneDay_ass = new DayOfWeek();
        oneDay_ass.setTitle("   Отсутствует подключение к серверу!");
        oneDay_ass.setCab("");
        DayOfWeek_List.add(oneDay_ass);
        adapter.notifyDataSetChanged();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, setting_1_ext, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    DayOfWeek_List.clear();
                    JSONObject jsonObject = response.getJSONObject("data");
                    JSONArray jsonArray = jsonObject.getJSONArray("timetable");

                    Calendar cal = Calendar.getInstance();
                    int dayOfWeek;
                    JSONObject jsonObject_day;


                    if (cal.get(Calendar.HOUR_OF_DAY) >= 15 && cal.get(Calendar.DAY_OF_WEEK) == 7) {           // Расписание на понедельник в субботу
                        dayOfWeek = 0;

                        jsonObject_day = jsonArray.getJSONObject(dayOfWeek);
                        DayOfWeek oneDay = new DayOfWeek();

                        oneDay.setTitle("Расписание на послезавтра (" + jsonObject_day.getString("day") + ").");                           //Шапка view
                        oneDay.setCab("");
                        DayOfWeek_List.add(oneDay);

                    } else if (cal.get(Calendar.HOUR_OF_DAY) >= 15 || cal.get(Calendar.DAY_OF_WEEK) == 1) {
                        dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 1;

                        jsonObject_day = jsonArray.getJSONObject(dayOfWeek);
                        DayOfWeek oneDay = new DayOfWeek();

                        oneDay.setTitle("Расписание на завтра (" + jsonObject_day.getString("day") + ").");                           //Шапка view
                        oneDay.setCab("");
                        DayOfWeek_List.add(oneDay);
                    } else {
                        dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 2;

                        jsonObject_day = jsonArray.getJSONObject(dayOfWeek);
                        DayOfWeek oneDay = new DayOfWeek();

                        oneDay.setTitle("Расписание на сегодня (" + jsonObject_day.getString("day") + ").");                           //Шапка view
                        oneDay.setCab("");
                        DayOfWeek_List.add(oneDay);
                    }

                    if (dayOfWeek != 5) {
                        JSONArray jsonTimeWeekdays = jsonObject.getJSONArray("weekdays");
                        knockTimeLength = jsonTimeWeekdays.length() * 2 - 1;
                        for (int i = 0; i < jsonTimeWeekdays.length(); i++) {
                            knockTime[i * 2] = toMins(jsonTimeWeekdays.getJSONArray(i).getString(0));
                            knockTime[(i * 2) + 1] = toMins(jsonTimeWeekdays.getJSONArray(i).getString(1));
                        }
                    } else {
                        JSONArray jsonTimeWeekend = jsonObject.getJSONArray("weekend");
                        knockTimeLength = jsonTimeWeekend.length() * 2 - 1;
                        for (int i = 0; i < jsonTimeWeekend.length(); i++) {
                            knockTime[i * 2] = toMins(jsonTimeWeekend.getJSONArray(i).getString(0));
                            knockTime[(i * 2) + 1] = toMins(jsonTimeWeekend.getJSONArray(i).getString(1));
                        }
                    }

                    JSONArray jsonArray_lessons = jsonObject_day.getJSONArray("lessons");
                    for (int j = 0; j < jsonArray_lessons.length(); j++) {

                        JSONObject jsonObject_lesson = jsonArray_lessons.getJSONObject(j);

                        DayOfWeek oneDay_ = new DayOfWeek();
                        lessons[j] = jsonObject_lesson.getString("subject");
                        cabines[j] = jsonObject_lesson.getString("cab");
                        oneDay_.setTitle(Integer.toString(j + 1) + ".  " + lessons[j]);           //Урок
                        oneDay_.setCab(cabines[j]);                                                     //Кабинет

                        DayOfWeek_List.add(oneDay_);

                        adapter.notifyDataSetChanged();
                        progressDialog.dismiss();
                    }

                    adapter.notifyDataSetChanged();
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }
                    final long cacheHitButRefreshed = 10 * 60 * 1000; // in ? minutes cache will be hit, but also refreshed on background
                    final long cacheExpired = 24 * 24 * 60 * 60 * 1000; // in ? hours this cache entry expires completely
                    long now = System.currentTimeMillis();
                    final long softExpire = now + cacheHitButRefreshed;
                    final long ttl = now + cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(new JSONObject(jsonString), cacheEntry);
                } catch (UnsupportedEncodingException | JSONException e) {
                    return Response.error(new ParseError(e));
                }
            }

            @Override
            protected void deliverResponse(JSONObject response) {
                super.deliverResponse(response);
            }

            @Override
            public void deliverError(VolleyError error) {
                progressDialog.dismiss();
                super.deliverError(error);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                return super.parseNetworkError(volleyError);
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        final int id = item.getItemId();
        drawer.closeDrawers();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (id) {
                    case R.id.nav_time:
                        drawer.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.nav_sc:
                        startActivity(new Intent(TimeActivity.this, ScheduleActivity.class));
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.nav_kn:
                        startActivity(new Intent(TimeActivity.this, KnockActivity.class));
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.nav_teach:
                        startActivity(new Intent(TimeActivity.this, TeachersActivity.class));
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.nav_website:
                        startActivity(new Intent(TimeActivity.this, WebsiteActivity.class));
                        overridePendingTransition(0, 0);
                        break;
                    case R.id.nav_settings:
                        startActivity(new Intent(TimeActivity.this, SettingsActivity.class));
                        break;
                }
            }
        }, 250);    //Задержка

        return true;
    }


    class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            Log.e("TimeActivity", "Обновление");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Calendar cal = Calendar.getInstance();
                    int systemTimeInMin = (cal.get(Calendar.MINUTE)) + (60 * cal.get(Calendar.HOUR_OF_DAY));
                    int systemDayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

                    //Log.e("Test", Integer.toString(knockTime[knockTimeLength]));

                    if (systemDayOfWeek == 1) {
                        textView_1.setText("Воскресенье, отдыхай!");
                        textView_2.setText("");
                    } else if (systemTimeInMin <= knockTime[knockTimeLength]) {

                        for (int i = 0; i <= knockTimeLength; i++) {
                            if (knockTime[i] >= systemTimeInMin) {
                                textView_1.setText("Звонок через " + Integer.toString((knockTime[i] - systemTimeInMin)) + " мин.");

                                if (setting_2_def) {
                                    if (i % 2 == 0) {
                                        textView_2.setText("Сейчас: Перемена." + System.getProperty("line.separator") + "Затем: " + lessons[i / 2] + " (" + cabines[i / 2] + ")");
                                    } else {
                                        textView_2.setText("Сейчас: " + lessons[(i / 2)] + " (" + cabines[(i / 2)] + ")" + System.getProperty("line.separator") +
                                                "Затем: Перемена." + System.getProperty("line.separator") +
                                                "Следом: " + lessons[(i / 2) + 1] + " (" + cabines[(i / 2) + 1] + ")");
                                    }
                                } else {
                                    if (i % 2 == 0) {
                                        textView_2.setText("Следующее: " + lessons[i / 2] + " (" + cabines[i / 2] + ")");
                                    } else {
                                        textView_2.setText("Следующее: Перемена." + System.getProperty("line.separator") + "Затем: " + lessons[(i / 2) + 1] + " (" + cabines[(i / 2) + 1] + ")");
                                    }
                                }
                                break;
                            }
                        }
                    } else {
                        textView_1.setText("Звонок через ∞ минут.");
                        textView_2.setText("");
                    }

                }
            });

        }
    }

    private int toMins(String s) {
        String[] hourMin = s.split(":");
        int hour = Integer.parseInt(hourMin[0]);
        int mins = Integer.parseInt(hourMin[1]);
        int hoursInMins = hour * 60;
        return hoursInMins + mins;
    }

    public void dialogUpgrade() {

        if (preferences.getBoolean("Setting_1", true)) {
            JsonObjectRequest jsonRequest_ = new JsonObjectRequest
                    (Request.Method.GET, setting_3_ext, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response_) {
                            try {
                                String version = response_.getString("version");
                                if (!(getString(R.string.version).equals(version))) {
                                    CustomDialogFragment dialog = new CustomDialogFragment();
                                    dialog.show(getSupportFragmentManager(), "custom");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });

            Volley.newRequestQueue(this).add(jsonRequest_);
        }
    }


}

