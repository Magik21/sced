package com.example.tim_6.sced;

public class DayOfWeek {

    public String title;
    public String cab;

    public DayOfWeek() {
    }

    public DayOfWeek(String title, String cab) {
        this.title = title;
        this.cab = cab;
    }

    public String getTitle() {
        return title;
    }

    public String getCab() {
        return cab;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCab(String cab) {
        this.cab = cab;
    }

}