package com.example.tim_6.sced;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;

public class CustomDialogFragment extends DialogFragment {

    SharedPreferences preferences;
    String setting_4_ext;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
       AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
       preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
       setting_4_ext = preferences.getString("extra_Setting_4",getString(R.string.url_mobile));

       builder.setMessage("Вышла новая версия приложения. Загрузить сейчас?")
            .setTitle("Доступна новая версия!")
            .setPositiveButton("Обновить", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                Uri address = Uri.parse(setting_4_ext);
                Intent openlinkIntent = new Intent(Intent.ACTION_VIEW, address);
                startActivity(openlinkIntent);
                }
            })
            .setNegativeButton("Не сейчас", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

       return builder.create();
    }
}